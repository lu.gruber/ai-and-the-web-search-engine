import indexing
from whoosh.qparser import QueryParser
import traceback
from flask import Flask, request, render_template

# getting the index and searching it for results, then appending them to the "results" list
def search_index(keyword: str):
    ix = indexing.get_index()
    with ix.searcher() as searcher:
        result_list = []
        query = QueryParser("content", ix.schema).parse(keyword)
        results = searcher.search(query, limit=100)
        results.fragmenter.surround = 50
        for r in results:
            if r.highlights("content"):  # filtering results that have no contents and are just embedded on the site
                result_list.append((r.fields()["url"], r.highlights("content"), r.fields()["title"]))
        return result_list, len(results)


app = Flask(__name__)

# including an errorhandler
@app.errorhandler(500)
def internal_error(exception):
   return "<pre>"+traceback.format_exc()+"</pre>"

# rendering the template and giving it the right attributes
@app.route("/")
def start():
    search_word = request.args.get('search_word')
    if search_word is None:
        return render_template("start.html", title="start", result="start")
    else:
        return render_template("start.html", title=search_word, search=search_word, result=search_index(search_word))