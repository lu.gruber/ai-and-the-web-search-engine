import os

from whoosh.fields import *
from whoosh.index import create_in
from whoosh import index

# creating index schema
ix_schema = Schema(title=TEXT(stored=True), url=ID(stored=True, unique=True), content=TEXT(stored=True), date=DATETIME(stored=True))

# Create an index if not created or open an existing one, then return it
def get_index():
    if not os.path.exists("indexdir"):
        os.makedirs("indexdir")
        return create_in("indexdir", schema=ix_schema)
    return index.open_dir("indexdir")